package com.example.uapv1900315.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends ArrayAdapter<String> {

    CountryList cl;
    public MyAdapter(Context context, String[] countries) {

        super(context, 0, countries);

    }



    @Override

    public View getView(int position, View view, ViewGroup parent) {

        String countryname =(String) getItem(position);



        if (view == null) {

            view = LayoutInflater.from(getContext()).inflate(R.layout.my_item, parent, false);


        }
        final Country country = cl.getCountry(countryname);


        ImageView image = (ImageView) view.findViewById(R.id.imageC);

        TextView countryNmae = (TextView) view.findViewById(R.id.countryN);


        int id = view.getResources().getIdentifier("com.example.uapv1900315.myapplication:drawable/" +country.getmImgFile() , null, null);
        image.setImageResource(id);

        countryNmae.setText(countryname);


        return view;

    }

}