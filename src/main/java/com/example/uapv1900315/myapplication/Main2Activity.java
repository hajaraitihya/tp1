package com.example.uapv1900315.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    CountryList cl;
    ImageView image;
    TextView countryname;
    EditText capitale;
    EditText langue;
    EditText monnaie;
    EditText population;
    EditText sup;
    Button sauvgarde;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cl= new CountryList();
        setContentView(R.layout.activity_main2);
        image=(ImageView) findViewById(R.id.imageView);
        countryname=(TextView) findViewById(R.id.country);
        langue=(EditText) findViewById(R.id.langue);
        capitale=(EditText) findViewById(R.id.capitale);
        monnaie=(EditText) findViewById(R.id.monnaire);
        population=(EditText) findViewById(R.id.Population);
        sup=(EditText) findViewById(R.id.Sup);
        sauvgarde=(Button) findViewById(R.id.button);
        Bundle extras = getIntent().getExtras();


        final Country country = cl.getCountry(extras.getString("country"));


        int id = getResources().getIdentifier("com.example.uapv1900315.myapplication:drawable/" +country.getmImgFile() , null, null);
        image.setImageResource(id);
        countryname.setText(extras.getString("country"));
        capitale.setText(country.getmCapital());
        langue.setText(country.getmLanguage());
        monnaie.setText(country.getmCurrency());

        population.setText(String.valueOf(country.getmPopulation()));
       sup.setText(String.valueOf(country.getmArea()));

        sauvgarde.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                country.setmCapital(capitale.getText().toString());
                country.setmLanguage(langue.getText().toString());
                //Toast.makeText("jfhf",,,);
            }
        });

    }
}
