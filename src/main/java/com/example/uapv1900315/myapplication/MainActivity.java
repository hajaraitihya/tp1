package com.example.uapv1900315.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    ListView mylist;
    CountryList cl;
    private String[] pays;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pays = cl.getNameArray();
        mylist = (ListView) findViewById(R.id.mylist);
        cl= new CountryList();

       // HashMap<String, Country> hashMap = cl.getCountry();

        final MyAdapter adapter = new MyAdapter(this,pays);
        mylist.setAdapter(adapter);


        mylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                    Intent myIntent = new Intent(view.getContext(), Main2Activity.class);
                   String countryname =(String) (mylist.getItemAtPosition(position));
                   myIntent.putExtra("country", countryname);
                   startActivityForResult(myIntent, 0);





            }
        });
    }
}
